# -*- coding: utf-8 -*-

__author__ = 'Maxim Kolyubyakin'


from os import environ

from twisted.internet.defer import inlineCallbacks
from twisted.internet import error

from autobahn.twisted.util import sleep
from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner

NUMBER_OF_NODES = 6


class Component(ApplicationSession):

    @inlineCallbacks
    def onJoin(self, details):
        print("Session attached")
        while True:
            for i in xrange(1, NUMBER_OF_NODES+1):
                NODE_ID = str(i)
                print 'Publishing to com.iot.nodes - node: ' + NODE_ID
                self.publish(u'com.iot.nodes', NODE_ID)
            yield sleep(4)


runner = ApplicationRunner(
    environ.get("iot", u"ws://127.0.0.1:8080/ws"),
    u"iot",
    debug_wamp=False,
    debug=False,
)

try:
    runner.run(Component)
except error.ConnectionRefusedError:
    print 'No connection to WAMP Router'
