# -*- coding: utf-8 -*-

__author__ = 'Maxim Kolyubyakin'

import os
from datetime import datetime

from twisted.web.static import File
from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.internet import reactor, error
from twisted.internet.defer import inlineCallbacks

from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner


TIMEOUT = 5
CONNECTED = "Node is connected"
DISCONNECTED = "Node is disconnected"
WORKING = "Node is visible"

current_dir = os.path.dirname(os.path.abspath(__file__))
connected_nodes = {}
session = None


def _check(node, app_session):
    if connected_nodes[node]:
        now = datetime.now()
        delta = (now - connected_nodes[node]).total_seconds()
        if delta > TIMEOUT:
            connected_nodes[node] = None
            app_session.publish(u'com.iot.state', node, 0)
            message = 'Node ' + node + ': ' + DISCONNECTED
            print message


class Notify(Resource):

    isLeaf = True

    def render_GET(self, request):
        params = request.args
        message = ''
        if 'node' in params:
            node = params['node'][0]
            if node in connected_nodes:
                if connected_nodes[node]:
                    connected_nodes[node] = datetime.now()
                    session.publish(u'com.iot.state', node, 1)
                    message = 'Node ' + node + ': ' + WORKING
                else:
                    connected_nodes[node] = datetime.now()
                    session.publish(u'com.iot.state', node, 1)
                    message = 'Node ' + node + ': ' + CONNECTED
            else:
                connected_nodes[node] = datetime.now()
                session.publish(u'com.iot.state', node, 1)
                message = 'Node ' + node + ': ' + CONNECTED

            reactor.callLater(TIMEOUT, _check, node, session)
        print message
        return message


class Root(Resource):

    isLeaf = False

    def getChild(self, path, request):
        if path == '':
            return self
        return Resource.getChild(self, path, request)

    def render_GET(self, request):
        with open(current_dir + '/web/index.html') as index_page:
            data = index_page.read()
        return data


class Component(ApplicationSession):

    @inlineCallbacks
    def onJoin(self, details):
        global session
        session = self
        print("Session attached")
        yield self.subscribe(self.on_event, u'com.iot.nodes')
        yield self.register(self.all_nodes, u'com.iot.allnodes')

    def on_event(self, node):
        global connected_nodes
        message = ''
        if node in connected_nodes:
            if connected_nodes[node]:
                connected_nodes[node] = datetime.now()
                self.publish(u'com.iot.state', node, 1)
                message = 'Node ' + node + ': ' + WORKING
            else:
                connected_nodes[node] = datetime.now()
                self.publish(u'com.iot.state', node, 1)
                message = 'Node ' + node + ': ' + CONNECTED
        else:
            connected_nodes[node] = datetime.now()
            self.publish(u'com.iot.state', node, 1)
            message = 'Node ' + node + ': ' + CONNECTED

        reactor.callLater(TIMEOUT, _check, node, self)
        print message

    def all_nodes(self):
        global connected_nodes
        return_list = []
        for key, value in connected_nodes.items():
            if value:
                return_list.append((key, 1))  # 1 = Connected and working
            else:
                return_list.append((key, 0))  # 0 = Disconnected
        return return_list


root = Root()
root.putChild('static', File(current_dir+'/web'))
root.putChild('notify', Notify())

factory = Site(root)
reactor.listenTCP(9000, factory)

runner = ApplicationRunner(
    os.environ.get("iot", u"ws://127.0.0.1:8080/ws"),
    u"iot",
    debug_wamp=False,
    debug=False,
)

try:
    runner.run(Component)

except error.ConnectionRefusedError:
    print 'No connection to WAMP Router'
