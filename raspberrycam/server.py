# -*- coding: utf-8 -*-

__author__ = 'Maxim Kolyubyakin'

import os
import io
import numpy as np
import cv2

import datetime

from twisted.internet import error
from twisted.internet.defer import inlineCallbacks, DeferredLock

from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import Column, Integer, String, DateTime

from pysqlite2 import dbapi2 as sqlite

Base = declarative_base()
engine = create_engine("sqlite+pysqlite:///store.db", module=sqlite)
SessionFactory = sessionmaker(bind=engine, autocommit=True)
Session = scoped_session(SessionFactory)
session = Session()

current_dir = os.path.dirname(os.path.abspath(__file__))


class Row(Base):

    __tablename__ = 'row'

    id = Column(Integer, primary_key=True)
    path = Column(String)
    date_time = Column(DateTime)


class Component(ApplicationSession):

    cameras = {}
    ready_cameras = set()
    current_picture_path = ""
    _lock = DeferredLock()

    @inlineCallbacks
    def onJoin(self, details):
        print "Server session attached"

        yield self.subscribe(self.on_camera_connected, u'com.quazar.camcon')
        yield self.subscribe(self.trigger_start_photos, u'com.quazar.trigger')

    @inlineCallbacks
    def on_camera_connected(self, camera_id, personal_link):
        self.cameras[camera_id] = personal_link
        yield self.subscribe(self.get_camera_data, personal_link)

        print "GOT: Raspberry pi connected: id " + camera_id

    def trigger_start_photos(self):
        self.ready_cameras.clear()

        current_time = datetime.datetime.now()
        self.current_picture_path = current_dir+'/photos/'+current_time.strftime("%d%m%y_%H%M%S")
        os.mkdir(self.current_picture_path)
        new_row = Row(
            path=self.current_picture_path,
            date_time=current_time
        )
        session.add(new_row)
        session.flush()
        self.publish(u'com.quazar.startphoto')

    def onDisconnect(self):
        print "Disconnected"

    def get_camera_data(self, _id, data):
        self.ready_cameras.add(_id)
        print "Photo from", _id
        filename = self.current_picture_path+'/'+ _id + '.jpg'
        istream = io.BytesIO()
        istream.write(data)
        istream.seek(0)
        nparr = np.fromstring(istream.getvalue(), np.uint8)
        img_np = cv2.imdecode(nparr, 1)
        cv2.imwrite(filename,img_np)

        # def filelock(filename):
        #     f = open(filename, 'w')
        #     f.write(id + data)
        #     f.close()
        #
        # filelock(filename)
        #self._lock.run(filelock,filename)

        if len(self.ready_cameras) == len(self.cameras):
            print "Full row"
            self.ready_cameras.clear()
            self.trigger_start_photos()


runner = ApplicationRunner(
    os.environ.get("QUAZAR", u"ws://192.168.0.100:8080/ws"),
    u"quazar",
    debug_wamp=False,
    debug=False,
)

Base.metadata.create_all(engine)

try:
    runner.run(Component)

except error.ConnectionRefusedError:
    print 'No connection to WAMP Router'
