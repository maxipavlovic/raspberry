# -*- coding: utf-8 -*-

__author__ = 'Maxim Kolyubyakin'


from os import environ

from twisted.internet.defer import inlineCallbacks
from twisted.internet import error

from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner
from autobahn.twisted.util import sleep

CAMERA_ID = '2'
PERSONAL_LINK = u'com.quazar.cam2'


class Component(ApplicationSession):

    @inlineCallbacks
    def onJoin(self, details):
        print "Client session attached"

        print "SENDING: I'm connected: id " + CAMERA_ID
        self.publish(u'com.quazar.camcon', CAMERA_ID, PERSONAL_LINK)
        yield self.subscribe(self.start_photo, u'com.quazar.startphoto')

    @inlineCallbacks
    def start_photo(self):
        print "GOT: Make a photo"
        yield sleep(1)
        self.publish(PERSONAL_LINK, CAMERA_ID, "Photo made")


runner = ApplicationRunner(
    environ.get("QUAZAR", u"ws://127.0.0.1:8080/ws"),
    u"quazar",
    debug_wamp=False,
    debug=False,
)

try:
    runner.run(Component)
except error.ConnectionRefusedError:
    print 'No connection to WAMP Router'
