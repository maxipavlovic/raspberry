# -*- coding: utf-8 -*-

__author__ = 'Maxim Kolyubyakin'

import io
import picamera
from os import environ

from twisted.internet.defer import inlineCallbacks
from twisted.internet import error

from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner
from autobahn.twisted.util import sleep

CAMERA_ID = '1'
PERSONAL_LINK = u'com.quazar.cam1'

camera = picamera.PiCamera()
camera.resolution = (1296, 972)
camera.shutter_speed = 5000


class Component(ApplicationSession):

    camera = None

    @inlineCallbacks
    def onJoin(self, details):
        with picamera.PiCamera() as _camera:
            self.camera = _camera
        print "Client session attached"
        self.camera.start_preview()
        yield sleep(2)
        print "SENDING: I'm connected: id " + CAMERA_ID
        self.publish(u'com.quazar.camcon', CAMERA_ID, PERSONAL_LINK)
        yield self.subscribe(self.start_photo, u'com.quazar.startphoto')

    @inlineCallbacks
    def start_photo(self):
        print "GOT: Make a photo"
        stream = io.BytesIO()
        self.camera.capture(stream, "jpeg")
        stream.seek(0)
        self.publish(PERSONAL_LINK, CAMERA_ID, stream.read())
        stream.seek(0)
        stream.truncate()


runner = ApplicationRunner(
    environ.get("QUAZAR", u"ws://127.0.0.1:8080/ws"),
    u"quazar",
    debug_wamp=False,
    debug=False,
)

try:
    runner.run(Component)
except error.ConnectionRefusedError:
    print 'No connection to WAMP Router'
