# -*- coding: utf-8 -*-

__author__ = 'Maxim Kolyubyakin'


from os import environ

from twisted.internet.defer import inlineCallbacks
from twisted.internet import error

from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner


class Component(ApplicationSession):

    def onJoin(self, details):
        print "Started"
        self.publish(u'com.quazar.trigger')


runner = ApplicationRunner(
    environ.get("QUAZAR", u"ws://192.168.0.100:8080/ws"),
    u"quazar",
    debug_wamp=False,
    debug=False,
)

try:
    runner.run(Component)
except error.ConnectionRefusedError:
    print 'No connection to WAMP Router'
